<?php
    defined( 'ABSPATH' ) || exit;

 function pesquisa_avancada_rendering($atts) {
    ?>
    <style>
       #short_pesquisa{
	        display: flex;
	        justify-content: space-evenly;
            align-items: center;
            font-size: 0.8rem;
            height: 99px;
            background-color: #89005c;
        }
        #short_pesquisa input, #short_pesquisa select {
            height: 35px;
            border-radius: 3px;
            -moz-border-radius: 3px;
            color: #75758b;

        }
        #short_pesquisa button{
            height: 35px;
            border-radius: 3px;
            -moz-border-radius: 3px;
            padding: 0 2%;
            font-size: 0.8rem;
            font-weight: bold;
            background-color: #ffa21b;
            color: #032d2c;

        }
        .form_part button{
            line-height: 1em;
        }
        .result-info{
            border: 1px solid #ccc;
            padding: 2% 0% 0% 5%;
        }
        #mobile_filters, #action_search{
            display: flex;
            width: 50%;
            justify-content: space-evenly;
        }
       @media all and (max-width: 768px){
             #mobile_filters{
                display: flex;
                position:absolute;
                margin-top: -40px;
                /* left: 10%; */
                width: 93%;
            }
            #action_search{
                display: flex;
                position:absolute;
                /* top:50px; */
                margin-top: 50px;
                /* left: 10%; */
                width:100%;
            }
       }
    </style>
    <div id="short_pesquisa">
    <div id="mobile_filters">
    <div class="form_part">
    <form>
        <select class="pesquisa_tipos" id="pesquisa_tipos" name="pesquisa_tipos" required>
        <?php
            echo '<option selected value="" readonly="true">Tipo de curso</option>';
            $tags = get_terms(array(
                'taxonomy' => 'pesquisa_tipos',
                'orderby' => 'name',
                'hide_empty' => true // for development
            ));
            if ( $tags ) {
                foreach( $tags as $tag ) {
                    printf('<option value="%1$s">%2$s</option>',
                        $tag->term_id,
                        $tag->name
                    );
                    
                }
            }
        ?>
        </select>
    </div>
    <div class="form_part">
        <select class="pesquisa_localizacao" id="pesquisa_localizacao" name="pesquisa_localizacao" required>
        <?php
            echo '<option selected value="" readonly="true">UF</option>';
            $tags = get_tags(array(
                'taxonomy' => 'pesquisa_localizacao',
                'orderby' => 'name',
                'hide_empty' => true // for development
            ));
            if ( $tags ) {
                foreach( $tags as $tag ) {
                    printf('<option value="%1$s">%2$s</option>',
                        $tag->term_id,
                        $tag->name
                    );
                    
                }
            }
        ?>
        </select>
    </div>
    <div class="form_part">
        <select class="pesquisa_instituicao" id="pesquisa_instituicao" name="pesquisa_instituicao" required>
        <?php
            echo '<option selected value="" readonly="true">Instituição</option>';
            $tags = get_tags(array(
                'taxonomy' => 'pesquisa_instituicao',
                'orderby' => 'name',
                'hide_empty' => true // for development
            ));
            if ( $tags ) {
                foreach( $tags as $tag ) {
                    printf('<option value="%1$s">%2$s</option>',
                        $tag->term_id,
                        $tag->name
                    );
                    
                }
            }
        ?>
        </select>
    </div>
    </div>
        <div id="action_search">
            <div class="form_part">
                <input type="text" name="nome_curso" id="input_curso_name" placeholder="Digite aqui o curso que busca">
            </div>
            <div class="form_part">
                <button id="do_curso_search">Encontrar Curso</button>
            </div>
        </div>
    </div>
    </form>

    <div class="results">

    </div>
    <?php
}
add_shortcode('pesquisa-cursos-avancada', 'pesquisa_avancada_rendering');

