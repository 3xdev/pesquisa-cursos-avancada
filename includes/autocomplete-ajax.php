<?php
add_action('wp_enqueue_scripts', function() {
    
	wp_enqueue_script('autocomplete-search', '/wp-content/plugins/pesquisa-cursos-avancada/resources/js/autocomplete.js', 
		['jquery', 'jquery-ui-autocomplete'], null, true);
	wp_localize_script('autocomplete-search', 'AutocompleteSearch', [
		'ajax_url' => admin_url('admin-ajax.php'),
		'ajax_nonce' => wp_create_nonce('autocompleteSearchNonce')
	]);
 
	$wp_scripts = wp_scripts();
	wp_enqueue_style('jquery-ui-css',
        '//ajax.googleapis.com/ajax/libs/jqueryui/' . $wp_scripts->registered['jquery-ui-autocomplete']->ver . '/themes/smoothness/jquery-ui.css',
        false, null, false
   	);
});
 
add_action('wp_ajax_nopriv_autocompleteSearch', 'awp_autocomplete_search');
add_action('wp_ajax_autocompleteSearch', 'awp_autocomplete_search');
function awp_autocomplete_search() {
	check_ajax_referer('autocompleteSearchNonce', 'security');
 
	$search_term = $_REQUEST['term'];
	if (!isset($_REQUEST['term'])) {
		echo json_encode([]);
	}
    if( isset($_REQUEST['tipo_args']) && !empty($_REQUEST['tipo_args']) ){
		$tipo_args = array(
			'taxonomy' => 'pesquisa_tipos',
			'field'    => 'term_id',
			'terms'    => $_REQUEST['tipo_args']
		);
	}
	if(isset($_REQUEST['instituicao_args']) && !empty($_REQUEST['instituicao_args']) ){
		$instituicao_args = array(
			'taxonomy' => 'pesquisa_instituicao',
			'field'    => 'term_id',
			'terms'    => $_REQUEST['instituicao_args']
		);
	}
	if(isset($_REQUEST['localizacao_args']) && !empty($_REQUEST['localizacao_args']) ){
		$localizacao_args = array(
			'taxonomy' => 'pesquisa_localizacao',
			'field'    => 'term_id',
			'terms'    => $_REQUEST['localizacao_args']
		);
	}
	$suggestions = [];
    if(isset($localizacao_args) || isset($instituicao_args) || isset($tipo_args)){

        $tax_query = array(
            'tax_query' => array(
                'relation' => 'AND',
                isset($tipo_args)?$tipo_args:'',
                isset($instituicao_args)?$instituicao_args:'',
                isset($localizacao_args)?$localizacao_args:'',
            )
        );
    }

    if(isset($tax_query)){
        $query = new WP_Query([
            's' => $search_term,
            'posts_per_page' => -1,
            'post_type' => 'cursos_pesquisa',
            'tax_query' => $tax_query,
            
        ]);
        error_log(json_encode($tax_query));
    }else{
        $query = new WP_Query([
            's' => $search_term,
            'posts_per_page' => -1,
            'post_type' => 'cursos_pesquisa',
            
        ]);
    }
	
	if ($query->have_posts()) {
		while ($query->have_posts()) {
			$query->the_post();
			$suggestions[] = [
				'id' => get_the_ID(),
				'label' => get_the_title(),
				'link' => get_post_meta( get_the_ID(), 'curso_url', true)
			];
		}
		wp_reset_postdata();
	}
	echo json_encode($suggestions);
	wp_die();
}