<?php
defined( 'ABSPATH' ) || exit;

add_action('wp_ajax_pesquisar_curso_ajax', 'pesquisar_curso_ajax');
add_action('wp_ajax_nopriv_pesquisar_curso_ajax', 'pesquisar_curso_ajax');
// print_r($_POST);
function pesquisar_curso_ajax() {

	if( isset($_POST['tipo_args']) && !empty($_POST['tipo_args']) ){
		$tipo_args = array(
			'taxonomy' => 'pesquisa_tipos',
			'field'    => 'term_id',
			'terms'    => $_REQUEST['tipo_args']
		);
	}
	if(isset($_POST['instituicao_args']) && !empty($_POST['instituicao_args']) ){
		$instituicao_args = array(
			'taxonomy' => 'pesquisa_instituicao',
			'field'    => 'term_id',
			'terms'    => $_REQUEST['instituicao_args']
		);
	}
	if(isset($_POST['localizacao_args']) && !empty($_POST['localizacao_args']) ){
		$localizacao_args = array(
			'taxonomy' => 'pesquisa_localizacao',
			'field'    => 'term_id',
			'terms'    => $_REQUEST['localizacao_args']
		);
	}
	if(isset($_POST['nome_curso'])){
		$search_term = $_POST['nome_curso'];
	}else{
		$search_term = '';
	}

    if(isset($localizacao_args) || isset($instituicao_args) || isset($tipo_args)){

        $tax_query = array(
            'tax_query' => array(
                'relation' => 'AND',
                isset($tipo_args)?$tipo_args:'',
                isset($instituicao_args)?$instituicao_args:'',
                isset($localizacao_args)?$localizacao_args:'',
            )
        );
    }

	if(isset($tax_query)){
        $query = new WP_Query([
            's' => $search_term,
            'posts_per_page' => -1,
            'post_type' => 'cursos_pesquisa',
            'tax_query' => $tax_query,
            
        ]);
    }else{
        $query = new WP_Query([
            's' => $search_term,
            'posts_per_page' => -1,
            'post_type' => 'cursos_pesquisa',
            
        ]);
    }
	error_log(json_encode($query));
	if ($query->have_posts()) {
		while ($query->have_posts()) {
			$query->the_post();
			$suggestions[] = [
				'id' => get_the_ID(),
				// 'label' => get_the_title(),
				// 'link' => get_post_meta( get_the_ID(), 'curso_url', true)
				
			];
		}
		wp_reset_postdata();
	}

	if( isset($suggestions) ){
		if(count($suggestions) == 1){
			$response['data_type'] = 'single';
			$response['data'] = (get_post_meta($suggestions[0]['id'], 'curso_url', true));
		}else{
			// $response['data_type'] = 'multiple';
			error_log('multiple sugestions are: '.json_encode($suggestions) );
			foreach ($suggestions as $suggestion) {
				$url = get_post_meta($suggestion['id'], 'curso_url', true);
				$title = get_the_title($suggestion['id'] );
				$tipo = get_post_meta($suggestion['id'], 'pesquisa_tipos', true);
				$instituicao = get_post_meta($suggestion['id'], 'pesquisa_instituicao', true);
				$localizacao = get_post_meta($suggestion['id'], 'pesquisa_localizacao', true);

				$response[] = '<a href="'.$url.'"><h3>'.$title.'</h3><p>'.$tipo.' em '.$instituicao.', '.$localizacao.'</p></a>';
			}
		}
	}else{
		$response['data_type'] = 'empty';
		$response['data'] = 'Nenhum curso encontrado';
	}
	echo json_encode($response);
	
	
	wp_die(); // this is required to terminate immediately and return a proper response

} 

