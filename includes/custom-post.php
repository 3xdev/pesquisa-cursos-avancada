<?php
defined( 'ABSPATH' ) || exit;

function tresx_pesquisa_avancada_custom_post_type() {
 
        // TODOS OS RÓTULOS DO PAINEL ADMIN
        $labels = array(
            'name'                => __( 'Cursos', 'Post Type General Name', 'twentytwenty' ),
            'singular_name'       => __( 'Curso', 'Post Type Singular Name', 'twentytwenty' ),
            'menu_name'           => __( 'Curso', 'twentytwenty' ),
            'parent_item_colon'   => __( 'Curso Pai', 'twentytwenty' ),
            'all_items'           => __( 'Todos os Cursos', 'twentytwenty' ),
            'view_item'           => __( 'Ver Cursos', 'twentytwenty' ),
            'add_new_item'        => __( 'Adicionar Novo Curso', 'twentytwenty' ),
            'add_new'             => __( 'Adicionar Novo', 'twentytwenty' ),
            'edit_item'           => __( 'Editar Curso', 'twentytwenty' ),
            'update_item'         => __( 'Atualizar Curso', 'twentytwenty' ),
            'search_items'        => __( 'Pesquisar Cursos', 'twentytwenty' ),
            'not_found'           => __( 'Nenhum curso encontrado, crie seu primeiro curso', 'twentytwenty' ),
            'not_found_in_trash'  => __( 'Lixo Não Encontrado', 'twentytwenty' ),
        );
         
        // OUTRAS OPCOES DO CPT
        $args = array(
            'label'               => __( 'cursos', 'twentytwenty' ),
            'description'         => __( 'Cursos cadastrados no sistema', 'twentytwenty' ),
            'labels'              => $labels,
            // SUPORTE
            'supports'            => array( 'title' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            // 'menu_position'       => 5,
            'menu-icon'           => 'dashicons-welcome-view-site',
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => false,
            'capability_type'     => 'post',
            'show_in_rest' => true,
            // metaboxes
            'register_meta_box_cb' => 'tresx_pesquisa_avancada_add_metaboxes',
        );
         
        // REGISTRA O CPT
    register_post_type( 'cursos_pesquisa', $args );
     
}
    /* Hook no 'init' action para executar a função
    */
     
add_action( 'init', 'tresx_pesquisa_avancada_custom_post_type', 0 );

function tresx_pesquisa_avancada_add_metaboxes() {

    
    add_meta_box(
        'dados_curso',
        'Dados do curso',
        'curso_form_data',
        'cursos_pesquisa',
        'normal',
        'default'
    );

    remove_meta_box('commentstatusdiv', 'cursos_pesquisa', 'normal');
    remove_meta_box('commentsdiv', 'cursos_pesquisa', 'normal');
    remove_meta_box('tagsdiv-pesquisa_instituicao', 'cursos_pesquisa', 'side');
    remove_meta_box('tagsdiv-pesquisa_tipos', 'cursos_pesquisa', 'side');
    remove_meta_box('pesquisa_localizacaodiv', 'cursos_pesquisa', 'side');

}
/**
 * HTML DA METABOX DO CLIENTE
 */
function curso_form_data() {
    global $post;
    
    // Nonce para validar o request
    wp_nonce_field( basename( __FILE__ ), 'woo_os__dados' );

    // Output the field
    echo '';
    ?>
    <label for="curso_url">URL do curso</label><br>
    <input type="URL" class="curso_url" id="curso_url" name="curso_url" style="width:100%" required
    value="<?php echo  get_post_meta(get_the_ID(), 'curso_url', true) ?>">

    <label for="pesquisa_tipos">Tipo de curso</label><br>
    <select class="pesquisa_tipos" id="pesquisa_tipos" name="pesquisa_tipos" style="width:100%" required>
    <?php
        $tags = get_tags(array(
            'taxonomy' => 'pesquisa_tipos',
            'orderby' => 'name',
            'hide_empty' => false // for development
        ));
        $query = new WP_Term_Query(array(
            'object_ids' =>  get_the_ID(),
            'fields'     => 'ids',
        ));

        $field_name = 'pesquisa_tipos';

        if(get_post_meta($post->ID,  $field_name, false)){
            $saved_tags = get_post_meta($post->ID,  $field_name, false);    
        }else{
            $saved_tags = ['1','2','3'];
        }
        if ( $tags ) {
            foreach( $tags as $tag ) {
                $selected = (in_array($tag->name, $saved_tags))
                    ?'selected'
                    :'';
                printf('<option %1$s value="%2$s">%3$s</option>',
                    $selected,
                    $tag->name,
                    $tag->name
                );
                
            }
        }
    ?>
    </select><br>
    <label for="pesquisa_instituicao">Instituição</label><br>
    <select class="pesquisa_instituicao" id="pesquisa_instituicao" name="pesquisa_instituicao" style="width:100%" required>
    <?php
        $tags = get_tags(array(
            'taxonomy' => 'pesquisa_instituicao',
            'orderby' => 'name',
            'hide_empty' => false // for development
        ));
        $query = new WP_Term_Query(array(
            'object_ids' =>  get_the_ID(),
            'fields'     => 'ids',
        ));

        $field_name = 'pesquisa_instituicao';

        if(get_post_meta($post->ID,  $field_name, false)){
            $saved_tags = get_post_meta($post->ID,  $field_name, false);    
        }else{
            $saved_tags = ['1','2','3'];
        }
        if ( $tags ) {
            foreach( $tags as $tag ) {
                $selected = (in_array($tag->name, $saved_tags))
                    ?'selected'
                    :'';
                printf('<option %1$s value="%2$s">%3$s</option>',
                    $selected,
                    $tag->name,
                    $tag->name
                );
                
            }
        }
    ?>
    </select><br>
    <label for="pesquisa_localizacao">Localização</label><br>
    <select class="pesquisa_localizacao" id="pesquisa_localizacao" name="pesquisa_localizacao" style="width:100%" required>
    <?php
        $tags = get_tags(array(
            'taxonomy' => 'pesquisa_localizacao',
            'orderby' => 'name',
            'hide_empty' => false // for development
        ));
        $query = new WP_Term_Query(array(
            'object_ids' =>  get_the_ID(),
            'fields'     => 'ids',
        ));

        $field_name = 'pesquisa_localizacao';

        if(get_post_meta($post->ID,  $field_name, false)){
            $saved_tags = get_post_meta($post->ID,  $field_name, false);    
        }else{
            $saved_tags = ['1','2','3'];
        }
        if ( $tags ) {
            foreach( $tags as $tag ) {
                $selected = (in_array($tag->name, $saved_tags))
                    ?'selected'
                    :'';
                printf('<option %1$s value="%2$s">%3$s</option>',
                    $selected,
                    $tag->name,
                    $tag->name
                );
                
            }
        }
    ?>
    </select><br>
<?php

}
// agora inserimos algumas taxonomias neste post
function tresx_pesquisa_avancada_instituicao_taxonomy() {
    // Labels part for the GUI
    $labels = array(
    'name' => _x( 'Instituições', 'taxonomy general name' ),
    'singular_name' => _x( 'Instituição', 'taxonomy singular name' ),
    'search_items' =>  __( 'Pesquisar Instituições' ),
    'popular_items' => __( 'Instituições Populares' ),
    'all_items' => __( 'Todas as Instituições' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Editar Instituição' ), 
    'update_item' => __( 'Atualizar Instituição' ),
    'add_new_item' => __( 'Adicionar Nova Instituição' ),
    'new_item_name' => __( 'Nome da Nova Instituição' ),
    'separate_items_with_commas' => __( 'Separe as Instituições por vírgula' ),
    'add_or_remove_items' => __( 'Adicione ou remova Instituições' ),
    'choose_from_most_used' => __( 'Escolher entre as Instituições mais usadas' ),
    'menu_name' => __( 'Instituições' ),
    ); 
     
    // Now register the non-hierarchical taxonomy like tag
    register_taxonomy('pesquisa_instituicao','cursos_pesquisa',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    // 'rewrite' => array( 'slug' => 'topic' ),
    ));
}
//HOOK no itin e cria a taxonomia
add_action( 'init', 'tresx_pesquisa_avancada_instituicao_taxonomy', 0 );

function tresx_pesquisa_avancada_tipo_de_curso_taxonomy() {
    // Labels part for the GUI
    $labels = array(
        'name' => _x( 'Tipos', 'taxonomy general name' ),
        'singular_name' => _x( 'Tipo', 'taxonomy singular name' ),
        'search_items' =>  __( 'Pesquisar Tipos' ),
        'popular_items' => __( 'Tipo Populares' ),
        'all_items' => __( 'Todos as Tipos' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Editar Tipo' ), 
        'update_item' => __( 'Atualizar Tipo' ),
        'add_new_item' => __( 'Adicionar Novo Tipo' ),
        'new_item_name' => __( 'Nome do Novo Tipo' ),
        'separate_items_with_commas' => __( 'Separe os tipos por vírgula' ),
        'add_or_remove_items' => __( 'Adicione ou remova Tipos' ),
        'choose_from_most_used' => __( 'Escolher entre os Tipos mais usados' ),
        'menu_name' => __( 'Tipos' ),
    ); 
        
    // Now register the non-hierarchical taxonomy like tag
    register_taxonomy('pesquisa_tipos','cursos_pesquisa',array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        // 'rewrite' => array( 'slug' => 'topic' ),
    ));
}
//HOOK no itin e cria a taxonomia de marcas para o tipo de post 'ordens_de_servico'
add_action( 'init', 'tresx_pesquisa_avancada_tipo_de_curso_taxonomy', 0 );

function tresx_pesquisa_avancada_localizacao_taxonomy() {
    // Labels part for the GUI
    $labels = array(
        'name' => _x( 'Localizações', 'taxonomy general name' ),
        'singular_name' => _x( 'Localização', 'taxonomy singular name' ),
        'search_items' =>  __( 'Pesquisar Localizações' ),
        'popular_items' => __( 'Localizações Populares' ),
        'all_items' => __( 'Todas as Localizações' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Editar Localização' ), 
        'update_item' => __( 'Atualizar Localização' ),
        'add_new_item' => __( 'Adicionar Nova Localização' ),
        'new_item_name' => __( 'Nome da Nova Localização' ),
        'separate_items_with_commas' => __( 'Separe as Localizações por vírgula' ),
        'add_or_remove_items' => __( 'Adicione ou remova Localizações' ),
        'choose_from_most_used' => __( 'Escolher entre as Localizações mais usadas' ),
        'menu_name' => __( 'Localizações' ),
    ); 
    // Now register the non-hierarchical taxonomy like tag
    register_taxonomy('pesquisa_localizacao','cursos_pesquisa',array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        // 'rewrite' => array( 'slug' => 'topic' ),
    ));
}
//HOOK no itin e cria a taxonomia de marcas para o tipo de post 'ordens_de_servico'
add_action( 'init', 'tresx_pesquisa_avancada_localizacao_taxonomy', 0 );


// salvando os dados
function pesquisa_curso_save__meta( $post_id, $post ) {

    global $wpdb, $post;
    // Cancela a edição se o usuário não tier a permissão correta
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    // VERIFICANDO SE O NONCE EXISTE ANTES DE POSTAR
    
    if ( !wp_verify_nonce( $_POST['woo_os__dados'], basename(__FILE__) ) ) {
        return $post_id;
    }
    

    if(isset($_POST['curso_url'])){
        $this_post_meta['curso_url'] = sanitize_text_field( $_POST['curso_url']);
    }
    if(isset($_POST['pesquisa_tipos'])){
        $this_post_meta['pesquisa_tipos'] = sanitize_text_field( $_POST['pesquisa_tipos']);
    }
    if(isset($_POST['pesquisa_instituicao'])){
        $this_post_meta['pesquisa_instituicao'] = sanitize_text_field( $_POST['pesquisa_instituicao']);
    }
    if(isset($_POST['pesquisa_localizacao'])){
        $this_post_meta['pesquisa_localizacao'] = sanitize_text_field( $_POST['pesquisa_localizacao']);
    }
    // META - FAZ O TRABALHO DURO DE PUBLICAR O CONTEUDO
    foreach ( $this_post_meta as $key => $value ) {

        // Don't store custom data twice
        if ( 'revision' === $post->post_type ) {
            return;
        }

        if ( get_post_meta( $post_id, $key, false ) === false ) {
            //Se a variável não estiver corretamente definida
            return;
        }

        if ( get_post_meta( $post_id, $key, false ) ) {
            //SE JA TIVER PREENCHIDO ATUALIZA
            update_post_meta( $post_id, $key, $value );
            
            // Ao salvar, além de salvar a informação como meta, salva como tag, isso vai permitir
            // que usemos vários tipos de pesquisa para filtrar os dados no futuro
            if(term_exists($value, $key)){
                wp_set_post_terms( $post_id, $value, $key, false );
                error_log('id: '.$post_id .' valor: '.$value.' taxonomia: '.$key);
            }
            
            
        } else {
            // SE ESTIVER EM BRANCO CRIA
            add_post_meta( $post_id, $key, $value );
            if(term_exists($value, $key)){
                wp_set_post_terms( $post_id, $value, $key, false );
                error_log('id: '.$post_id .' valor: '.$value.' taxonomia: '.$key);
            }
        }
        if ( ! $value ) {
            // SE APAGAR O CAMPO... APAGA
            delete_post_meta( $post_id, $key );
            unregister_taxonomy_for_object_type($key, 'cursos_pesquisa' );
        }

    }

   
}
add_action( 'save_post', 'pesquisa_curso_save__meta', 1, 2 );

add_filter('enter_title_here', 'my_title_place_holder' , 20 , 2 );
function my_title_place_holder($title , $post){

    if( $post->post_type == 'cursos_pesquisa' ){
        $my_title = "Nome do curso";
        return $my_title;
    }

    return $title;

}