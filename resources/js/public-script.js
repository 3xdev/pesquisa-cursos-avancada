function pesquisar_pelo_curso_ajax(){

    var tipo_args = jQuery( "#pesquisa_tipos" ).val();
    var instituicao_args = jQuery( "#pesquisa_instituicao" ).val();
    var localizacao_args = jQuery( "#pesquisa_localizacao" ).val();
    var nome_curso = jQuery( "#input_curso_name" ).val();
    // var term_args = jQuery( "#pesquisa_termo" ).val();

    var data = {
        'action': 'pesquisar_curso_ajax',
        'tipo_args': tipo_args,
        'instituicao_args': instituicao_args,
        'localizacao_args': localizacao_args,
        'nome_curso': nome_curso,
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    var response = jQuery.post(my_ajax_object.ajax_url, data, function(response) {
        if(response){
             
            var response = JSON.parse(response);

            if(response['data_type'] == 'single'){
                window.open(response['data'], '_blank');
            }else if(response['data_type'] == 'empty'){
                alert("Não encontramos nenhum curso com estes critérios");
            }else{
                jQuery('.results').empty()
                response.forEach(element => {
                    console.log(element);
                    jQuery('.results').append('<div class="result-info">'+ element+ '</div>');
                });
                // alert( JSON.stringify(response['data']) );
                

            }
            
        }

    });    
}
jQuery(function($){
    // multiple select with AJAX search
    $( "#do_curso_search" ).click(function(e) {
        e.preventDefault();
        pesquisar_pelo_curso_ajax();
      });

});

