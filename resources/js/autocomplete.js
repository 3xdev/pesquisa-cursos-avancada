jQuery(function($) {
	$('#input_curso_name').autocomplete({
		source: function(request, response) {

			var tipo_args = jQuery( "#pesquisa_tipos" ).val();
			var instituicao_args = jQuery( "#pesquisa_instituicao" ).val();
			var localizacao_args = jQuery( "#pesquisa_localizacao" ).val();

			$.ajax({
				dataType: 'json',
				url: AutocompleteSearch.ajax_url,
				data: {
					term: request.term,
					action: 'autocompleteSearch',
					security: AutocompleteSearch.ajax_nonce,
					'tipo_args': tipo_args,
					'instituicao_args': instituicao_args,
					'localizacao_args': localizacao_args,
				},
				success: function(data) {
					response(data);
				}
			});
		},
		select: function(event, ui) {
			// window.location.href = ui.item.link;
		},
		minLength: 0,
	});
});