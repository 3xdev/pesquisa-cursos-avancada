<?php
defined( 'ABSPATH' ) || exit;
/**
 * Plugin Name: Pesquisa de cursos Avançada
 * Plugin URI: https://3xweb.site
 * Description: Insere o tipo de post customizado Curso e suas taxonomias, além disso permite a exibição
 * de uma ferramenta de busca usando o shortcode [pesquisa-cursos-avancada] 
 * Author URI: https://3xweb.site/
 * Version: 1.0.0
 * Requires at least: 4.4
 * Tested up to: 5.7
 * Text Domain: tresx_pesquisa_avancada
 * Domain Path: /languages
 */

/**
 * Required minimums and constants
 */
define( 'TRESX_PESQUISA_AVANCADA__VERSION', '5.3.0' ); // WRCS: DEFINED_VERSION.
define( 'TRESX_PESQUISA_AVANCADA__MIN_PHP_VER', '5.6.0' );
define( 'TRESX_PESQUISA_AVANCADA__MIN_WC_VER', '3.0' );
define( 'TRESX_PESQUISA_AVANCADA__MAIN_FILE', __FILE__ );
define( 'TRESX_PESQUISA_AVANCADA__PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
define( 'TRESX_PESQUISA_AVANCADA__PLUGIN_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );

function TRESX_PESQUISA_AVANCADA_FUNCTION() {

	static $plugin;

	if ( ! isset( $plugin ) ) {

		class TRESX_PESQUISA_AVANCADA_CLASS {

			/**
			 * The *Singleton* instance of this class
			 *
			 * @var Singleton
			 */
			private static $instance;

			/**
			 * Returns the *Singleton* instance of this class.
			 *
			 * @return Singleton The *Singleton* instance.
			 */
			public static function get_instance() {
				if ( null === self::$instance ) {
					self::$instance = new self();
				}
				return self::$instance;
			}

			/**
			 * Private clone method to prevent cloning of the instance of the
			 * *Singleton* instance.
			 *
			 * @return void
			 */
			public function __clone() {}

			/**
			 * Private unserialize method to prevent unserializing of the *Singleton*
			 * instance.
			 *
			 * @return void
			 */
			public function __wakeup() {}

			/**
			 * Protected constructor to prevent creating a new instance of the
			 * *Singleton* via the `new` operator from outside of this class.
			 */
			public function __construct() {
				add_action( 'admin_init', [ $this, 'install' ] );

				
					if ( is_admin() ) {
						$this->admin_includes();
					}

					$this->includes();
					// add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );
	
			}
			/**
			 * Handles upgrade routines.
			 *
			 * @since 3.1.0
			 * @version 3.1.0
			 */
			public function install() {
				if ( ! is_plugin_active( plugin_basename( __FILE__ ) ) ) {
					return;
				}

			}
			private function includes() {
				// include_once dirname( __FILE__ ) . '/includes/';
				include_once dirname( __FILE__ ) . '/includes/custom-post.php';
				include_once dirname( __FILE__ ) . '/includes/shortcodes/barra-pesquisa.php';
				include_once dirname( __FILE__ ) . '/includes/autocomplete-ajax.php';
				
			}

			/**
			 * Admin includes.
			 */
			private function admin_includes() {
				// include_once dirname( __FILE__ ) . '/includes/custom-post.php';
				include_once dirname( __FILE__ ) . '/includes/custom-search.php';
				
			}

		}

		$plugin = TRESX_PESQUISA_AVANCADA_CLASS::get_instance();

	}

	return $plugin;
}
// carregando scripts adicionais
add_action( 'admin_enqueue_scripts', '_3xweb_pesquisa_avancada_scriptLoader' );

function _3xweb_pesquisa_avancada_scriptLoader(){
    // Register each script
    wp_register_script(
        'pesquisa_avancada_javascript', 
        plugins_url('/resources/js/admin.js', __FILE__ ), 
        array( 'jquery' ),
        false,
        true
    );
	
    wp_enqueue_script( 
    	'pesquisa_avancada_jquery_select2', 
    	'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', 
    	array('jquery'), 
    	false, 
    	true );

   
    wp_register_style('pesquisa_avancada_select2_css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
    wp_enqueue_script('pesquisa_avancada_javascript');
	wp_enqueue_style('pesquisa_avancada_select2_css');
    wp_enqueue_style('dashicons');
}
/**
 * Carregando scripts publicos
 */
function TRESX_PESQUISA_AVANCADA_publicScriptLoader($hook) {
 
    // create my own version codes
    $my_js_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . '/resources/js/' ));
     
    // 
    wp_enqueue_script( 'PCA_public_js', plugins_url( '/resources/js/public-script.js', __FILE__ ), array('jquery'), $my_js_ver );
	
    wp_localize_script( 'PCA_public_js', 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
 
}
add_action('wp_enqueue_scripts', 'TRESX_PESQUISA_AVANCADA_publicScriptLoader');
// iniciando o plugin e verificando as dependências
add_action( 'plugins_loaded', 'TRESX_PESQUISA_AVANCADA__init' );

function TRESX_PESQUISA_AVANCADA__init() {
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	load_plugin_textdomain( 'pesquisa-cursos-avancada', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
	TRESX_PESQUISA_AVANCADA_FUNCTION();
}